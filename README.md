# ExValorantAPIClient

ExValorantAPIClient is simple [Valorant API](https://developer.riotgames.com/apis) client.
  
## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `ex_valorant_api_client` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:ex_valorant_api_client, "~> 0.1.0"}
  ]
end
```
## Usage

```elixir
iex> client = ExValorantAPIClient.new("your_api_key")
%Tesla.Client{
    adapter: nil,
    fun: nil,
    post: [],
    pre: [
      {Tesla.Middleware.DecompressResponse, :call, [[]]},
      {Tesla.Middleware.JSON, :call, [[engine: Jason]]},
      {Tesla.Middleware.BaseUrl, :call, ["https://americas.api.riotgames.com"]},
      {Tesla.Middleware.Headers, :call, [[{"X-Riot-Token", "your_api_key"}]]}
    ]
}

iex> {:ok, ~~~} = ExValorantAPIClient.get_account_by_riot_id(client, "game_name", "#US1")
# If you changed region...
iex> {:ok, ~~~} = ExValorantAPIClient.get_account_by_riot_id(client, "game_name", "#JP1", region: "ASIA")
```