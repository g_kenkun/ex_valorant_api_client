defmodule ExValorantAPIClient.MixProject do
  use Mix.Project

  def project do
    [
      app: :ex_valorant_api_client,
      version: "0.1.0",
      elixir: "~> 1.0",
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      name: "ExValorantAPIClient",
      source_url: "https://gitlab.com/g_kenkun/ex_valorant_api_client"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {ExValorantAPIClient.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:hackney, "~> 1.15.2"},
      {:jason, ">= 1.0.0"},
      {:tesla, "~> 1.3.0"}
    ]
  end

  defp description do
    "Valorant API (https://developer.riotgames.com/apis#account-v1) simple wrapper."
  end

  defp package do
    [
      name: "ex_valorant_api_client",
      licenses: ["Apache-2.0"],
      links: %{"GitLab" => "https://gitlab.com/g_kenkun/ex_valorant_api_client"}
    ]
  end
end
