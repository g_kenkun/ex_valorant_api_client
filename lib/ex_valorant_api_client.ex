defmodule ExValorantAPIClient do
  @moduledoc """
  This module is [Valorant API](https://developer.riotgames.com/apis) wrapper public interface.

  ** Since the API is not yet available to the public, I have not confirmed that this module works properly. **
  """

  @doc """
  Create api client.

  ## Examples

  ```elixir
  iex> client = ExValorantAPIClient("your_api_key")
  %Tesla.Client{
    adapter: nil,
    fun: nil,
    post: [],
    pre: [
      {Tesla.Middleware.DecompressResponse, :call, [[]]},
      {Tesla.Middleware.JSON, :call, [[engine: Jason]]},
      {Tesla.Middleware.BaseUrl, :call, ["https://americas.api.riotgames.com"]},
      {Tesla.Middleware.Headers, :call, [[{"X-Riot-Token", "your_api_key"}]]}
    ]
  }
  ```

  This method sets the americas region by default. If you want to chang default region, please set opts.

  ```elixir
  client = ExValorantAPIClient.new("your_api_key", default_region: "asia") # or europe
  %Tesla.Client{
    adapter: nil,
    fun: nil,
    post: [],
    pre: [
      {Tesla.Middleware.DecompressResponse, :call, [[]]},
      {Tesla.Middleware.JSON, :call, [[engine: Jason]]},
      {Tesla.Middleware.BaseUrl, :call, ["https://asia.api.riotgames.com"]},
      {Tesla.Middleware.Headers, :call, [[{"X-Riot-Token", "your_api_key"}]]}
    ]
  }
  ```
  """

  @doc since: "0.1.0"

  @spec new(String.t()) :: Tesla.Client.t()
  def new(api_key, opts \\ []),
    do: ExValorantAPIClient.Connection.new(api_key, opts)

  @doc """
  Get Account data by puuid.

  ## Options

  - `region` - Change region.

  ## Example

  ```elixir
  ExValorantAPIClient.get_account_by_puuid(client, "your_puuid")
  ```
  """

  @doc since: "0.1.0"

  @spec get_account_by_puuid(Tesla.Client.t(), String.t(), keyword()) :: Tesla.Env.result()
  def get_account_by_puuid(client, puuid, opts \\ []),
    do: ExValorantAPIClient.Account.get_account_by_puuid(client, puuid, opts)

  @doc """
  Get Account data by game_name and tag_line.

  ## Options

  - `region` - Change region.

  ## Example
  ```
  # ExValorantAPIClient.get_account_by_riot_id(client, "your_game_name", "your_tag_line")
  umm...
  ```
  """

  @doc since: "0.1.0"

  @spec get_account_by_riot_id(Tesla.Client.t(), String.t(), keyword()) :: Tesla.Env.result()
  def get_account_by_riot_id(client, game_name, tag_line, opts \\ []),
    do: ExValorantAPIClient.Account.get_account_by_riot_id(client, game_name, tag_line, opts)

  @doc """
  Get active shard for a player.(???)

  ## Options

  - `region` - Change region.

  ## Example
  ExValorantAPIClient.get_active_shards_for_a_player(client, "val or lor", "your_puuid")
  """

  @doc since: "0.1.0"

  @spec get_active_shards_for_a_player(Tesla.Client.t(), String.t(), keyword()) ::
          Tesla.Env.result()
  def get_active_shards_for_a_player(client, game, puuid, opts \\ []),
    do: ExValorantAPIClient.Account.get_active_shards_for_a_player(client, game, puuid, opts)

  @doc """
  Get content optionally filtered by locale.

  ## Options

  - `region` - Change region.

  ## Example
  ExValorantAPIClient.get_content_optionally_filtered_by_locale(client)
  """

  @doc since: "0.1.0"

  @spec get_content_optionally_filtered_by_locale(Tesla.Client.t(), keyword()) ::
          Tesla.Env.result()
  def get_content_optionally_filtered_by_locale(client, opts \\ []),
    do: ExValorantAPIClient.Content.get_content_optionally_filtered_by_locale(client, opts)

  @doc """
  Get match by id.

  ## Options

  - `region` - Change region.

  ## Example
  ExValorantAPIClient.get_match_by_id(client, "match_id")
  """

  @doc since: "0.1.0"

  @spec get_match_by_id(Tesla.Client.t(), String.t(), keyword()) :: Tesla.Env.result()
  def get_match_by_id(client, match_id, opts \\ []),
    do: ExValorantAPIClient.Match.get_match_by_id(client, match_id, opts)

  @doc """
  Get metchlist for games played by puuid

  ## Options

  - `region` - Change region.

  ## Example
  ExValorantAPIClient.get_matchlist_for_games_played_by_puuid(client, "your_puuid")
  """

  @doc since: "0.1.0"

  @spec get_matchlist_for_games_played_by_puuid(Tesla.Client.t(), String.t(), keyword()) ::
          Tesla.Env.result()
  def get_matchlist_for_games_played_by_puuid(client, puuid, opts \\ []),
    do: ExValorantAPIClient.Match.get_matchlist_for_games_played_by_puuid(client, puuid, opts)
end
