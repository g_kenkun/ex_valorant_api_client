defmodule ExValorantAPIClient.Account do
  @moduledoc false

  import ExValorantAPIClient.Connection

  @spec get_account_by_puuid(Tesla.Client.t(), String.t(), keyword()) :: Tesla.Env.result()
  def get_account_by_puuid(client, puuid, opts \\ []) do
    path = "/riot/account/v1/accounts/by-puuid/" <> puuid
    process(client, path, opts)
  end

  @spec get_account_by_riot_id(Tesla.Client.t(), String.t(), keyword()) :: Tesla.Env.result()
  def get_account_by_riot_id(client, game_name, tag_line, opts \\ []) do
    path = "/riot/account/v1/accounts/by-riot-id/" <> game_name <> "/" <> tag_line
    process(client, path, opts)
  end

  @spec get_active_shards_for_a_player(Tesla.Client.t(), String.t(), keyword()) ::
          Tesla.Env.result()
  def get_active_shards_for_a_player(client, game, puuid, opts \\ []) do
    path = "/riot/account/v1/active-shards/by-game/" <> game <> "/by-puuid/" <> puuid
    process(client, path, opts)
  end
end
