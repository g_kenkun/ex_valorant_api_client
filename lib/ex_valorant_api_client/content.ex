defmodule ExValorantAPIClient.Content do
  @moduledoc false

  import ExValorantAPIClient.Connection

  @spec get_content_optionally_filtered_by_locale(Tesla.Client.t(), keyword()) ::
          Tesla.Env.result()
  def get_content_optionally_filtered_by_locale(client, opts \\ []) do
    path = "/val/content/v1/contents"
    process(client, path, opts)
  end
end
