defmodule ExValorantAPIClient.Connection do
  @moduledoc false

  use Tesla

  @spec new(String.t(), keyword()) :: Tesla.Client.t()
  def new(api_key, opts \\ []) do
    default_region = Keyword.get(opts, :default_region, "americas")

    Tesla.client([
      {Tesla.Middleware.DecompressResponse, []},
      {Tesla.Middleware.JSON, engine: Jason},
      {Tesla.Middleware.BaseUrl, "https://" <> default_region <> ".api.riotgames.com"},
      {Tesla.Middleware.Headers, [{"X-Riot-Token", api_key}]}
    ])
  end

  @spec process(Tesla.Client.t(), String.t(), keyword()) :: Tesla.Env.result()
  def process(client, path, opts \\ []) do
    path =
      case Keyword.get(opts, :region) do
        nil ->
          path

        region ->
          "https://" <> region <> ".api.riotgames.com" <> path
      end

    Tesla.get(client, path, opts: [path_params: opts])
  end
end
