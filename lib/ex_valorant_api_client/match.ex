defmodule ExValorantAPIClient.Match do
  @moduledoc false

  import ExValorantAPIClient.Connection

  @spec get_match_by_id(Tesla.Client.t(), String.t(), keyword()) :: Tesla.Env.result()
  def get_match_by_id(client, match_id, opts \\ []) do
    path = "/val/match/v1/matches/" <> match_id
    process(client, path, opts)
  end

  @spec get_matchlist_for_games_played_by_puuid(Tesla.Client.t(), String.t(), keyword()) ::
          Tesla.Env.result()
  def get_matchlist_for_games_played_by_puuid(client, puuid, opts \\ []) do
    path = "/val/match/v1/matchlists/by-puuid/" <> puuid
    process(client, path, opts)
  end
end
